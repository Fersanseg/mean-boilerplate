import { Request, Response } from "express";
import { GetSampleWelcome } from "../services/sample.service";

/* The controller calls a service function--which handles business logic-- and
handles the HTTP response appropriately. */
export function SampleWelcome(req: Request, res: Response) {
    GetSampleWelcome()
        .then(r => {
            res.status(200).json(r);
        })
        .catch(e => {
            console.error(e.message);
            res.sendStatus(500);
        })
}