import { Application } from "express";
import { SampleWelcome } from '../controllers/sample.controller';

/* Routes only redirect the request to the appropriate controller and delegates
any HTTP logic handling to it */
export class AppRoutes {

    // SAMPLE GET REQUEST
    public route(app:Application) {
        app.get('/', SampleWelcome);
    }
}