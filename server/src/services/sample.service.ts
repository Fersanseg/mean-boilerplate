import { GetWelcomeFromDB } from "../models/sample.model";



/* The service interacts with the database models, making a data request and 
applying logic to it before returning it to the controller. */
export async function GetSampleWelcome() {
    return new Promise((resolve, reject) => {
        try {
            GetWelcomeFromDB()
                .then((response: unknown) => {
                    resolve(response);
                });
        } catch (e) {
            reject(e);
        }
    })
}