import { DbConnect } from "./database";
import * as dotenv from 'dotenv';
import cors from "cors";
import express from "express";
import { AppRoutes } from './routes/routes';


dotenv.config();

const { DB_URI, SRV_PORT } = process.env;

if (!DB_URI) {
    console.error("No DB_URI variable was found in config.env");
    process.exit(1);
}
if (!SRV_PORT) {
    console.error("No SRV_PORT variable was found in config.env");
    process.exit(1);
}


const appRoutes = new AppRoutes();

const conn = new DbConnect(DB_URI);

conn.connection
    .then(() => {
        const app = express();
        app.use(cors());

        appRoutes.route(app);

        app.use(express.json());
        app.listen(SRV_PORT, () => {
            console.log(`Server listening at PORT = ${SRV_PORT}`);
        })
    })
    .catch(err => console.error(err));


// dbConnect(DB_URI)
//     .then(() => {
//         const app = express();
//         app.use(cors());

//         //appRoutes.route(app);
        
//         app.use(express.json());
//         app.listen(5200, () => {
//             console.log('Server listening at port 5200');
//         });
//     })
//     .catch(err => console.error(err));