import mongoose from 'mongoose';

export class DbConnect {
    public connection: Promise<any>;

    constructor(uri:string) {
        this.connection = this.GetDBConnection(uri);
    }

    private GetDBConnection(uri:string): Promise<any> {
        return mongoose.connect(uri)
                       .then(() => console.log('Connected to database'))
                       .catch(err => console.error(err));
    }
}