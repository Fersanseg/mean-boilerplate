import { model, Schema } from "mongoose";
import { ISample } from "../utils/isample";

// Mongoose schemas allow typescript to interface with the Mongo database
/* Schemas define the data structure of a Mongo document. By using typescript
   interfaces, we can make sure the schema's properties are all correct and
   prevent errors.*/
const sampleSchema = new Schema<ISample>({
    message: {
        type: String,
        required:true
    }
})

// Models represent the actual object (document) that is fetched from the db
const Welcome = model<ISample>(
    'WelcomeMessage', sampleSchema, 'SampleCollection');

export function GetWelcomeFromDB() {
    /* If we would have documents in our database, we would fetch them and
       return them. For the purposes of this sample, a dummy Promise is
       returned instead. */
    // return Welcome.find({});
    return new Promise(resolve => {
        resolve({"message": "Hello!"})
    })
}