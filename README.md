# MEAN stack with TypeScript

This is a boilerplate project for developing applications using the MEAN stack (MongoDB, Express, Angular, Node) together with TypeScript.
It includes a basic file system for organizing your server files, based on this article by Corey Cleary https://www.coreycleary.me/project-structure-for-an-express-rest-api-when-there-is-no-standard-way
The server is also preconfigured with a simple, dummy HTTP request pipeline. Once you are familiar with the way HTTP request logic is structured in this project, feel free to delete any file under the controllers, models, services and utils folders within "server/src", as well as the sample route in "server/src/routes/routes.ts".

## Notes

Personally, I am a fan of TailwindCSS, and I included it as a dependency in this project. If you don't want to use it, simply remove the dependency:
* After cloning this repository, go into the "client" folder and delete the "tailwindcss" line in the package.json file, under "devDependencies".
* Delete the `tailwind.config.js` file inside of the "client" folder.
* Edit the "client/src/styles.css" file and delete any line that starts with `@tailwind`.

The port in which your server listens for requests is 5200 by default. You can change this in the .env file within the "server" folder.

I added the .env file to this repository so that it is as much "plug and play" as possible. This, however, is a very bad practice. Env files are useful to store information that could be potentially sensitive, such as database connection strings, credentials, etc. As a general rule, do **NOT** upload your .env files to your own public repositories.

## Usage

To install this boilerplate and start developing your own app, just clone this repository in a local folder of your choice.
Then, install the necessary dependencies. 
* Run `npm run install-concurrently`. Concurrently is an utility that allows you to run several processes at the same time.
* Run `npm run install`. This uses Concurrently to install the dependencies of both the client and server modules of the app.
* Run `npm run start`. This starts the Mongo service, a server instance, and a client-side instance. If you want, you can automatically open the client in a browser tab by running `npm run start -o` instead.
